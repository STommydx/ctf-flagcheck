import numpy as np
from flag import flag
from comp4211 import sim


def flag_to_vec(flag):
    vec = np.array([ord(c) for c in flag], dtype=np.float)
    return vec


if __name__ == "__main__":
    flag_v = flag_to_vec(flag)
    print("Welcome to Flag Checker (FCer)!")
    print("Enter your flag and hyperparameter sigma to FC your flag!")
    print("Powered by COMP4211 similarity \"blackbox\".")
    print()
    
    while True:
        guess = input("flag? ")
        guess_v = flag_to_vec(guess)
        sigma = input("sigma? ")
        try:
            sigma = float(sigma)
        except ValueError:
            sigma = 0.2  # default value in instructions :)
        sigma = sigma * np.random.uniform(0.8, 1.2)  # everyone likes random <3
        if len(guess) != len(flag):
            print(f"Error: Incorrect Flag Length! (expect length {len(flag)})")
        else:
            print(f"{sim(flag_v, guess_v, sigma)[0]:.10f}")

