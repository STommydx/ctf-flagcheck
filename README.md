Flag Checked? (FCed?)
===

###### category: `reverse`

## Description

Nowadays, there are lot of fake news in the Internet. It is super important to FC your information, especially your flags! Luckily, the COMP 4211 TAs developed a highly advanced similarity measure - the "blackbox" similarity. Tommy, who got a FCed flag, wrote a simple checker based on similarity measure so you can FC by your own. :)

`nc chal.firebird.sh 12345`

Attachments: `chall.py`, `comp4211-asgn2.zip`


## Walkthrough

### Exploration

First, we try to go through what the flag checker `chall.py` is doing.

1. Import the real flag
2. Convert the flag to a float vector (array of ASCII)
3. Get the guess from user and convert to vector in the same way.
4. Get sigma hyperparameter from user and apply randomness.
5. Calculate similarity between two vectors using the blackbox and output to user.
6. Repeat steps 3 - 6.

There are two main difficulties of the task.

1. The similarity function is a blackbox
2. There is randomness applied


### Similarity Blackbox

How do we use the similarity blackbox? There is an instruction given in the assignment pdf. Put the binary in the same directory as the chall.py file and import it with the following statement.

```python=
from comp4211 import sim
```

With some experiments, the blackbox accepts numpy arrays of same dimension and returns a numpy array of dimension (1,). The returned value is between 0 and 1 and it is based on how "similar" the two array are.


### Is it a blackbox?

The filename gave us a lot of hints. The "blackbox" is written in Cython and the file extension is `.so`. Let's pull out our favourite decompiler Ghidra and decompile the code.

We can take a closer look in the `sim` function. Below is the most important part of the `sim` function extracted from the decompiled code.

```c
  else {
    __x = 0.00000000;
    iVar5 = 0;
    lVar7 = 0;
    do {
      iVar5 = iVar5 + 1;
      dVar11 = *(double *)(pcVar3 + lVar7 * 8) - *(double *)(pPVar8->data + lVar7 * 8);
      lVar7 = (long)iVar5;
      __x = __x + dVar11 * dVar11;
    } while (lVar7 != lVar6);
  }
  dims = 1;
  __x = __x * (double)(uVar1 ^ 0x8000000000000000);
  pPVar9 = (PyObject *)
           (*(code *)ppvVar10[0x5d])(__x,ppvVar10[2],1,dims,0xc,0,0,0,0,0,uVar14);
  pdVar4 = (double *)pPVar9[1].ob_refcnt;
  __x = exp(__x);
  *pdVar4 = __x;
  goto LAB_00100da0;
```

`iVar5`, `lVar6`, `lVar7` seems to be some index for looping through the array. `dVar11` is the difference of two doubles and it is squared and added to `__x`. The first guess would be it is related to the squared euclidean distance.

`uVar1` is sigma (can be found few lines above). `0x8000000000000000` is the MSB of a 64-bit. If you know double format well, you will immediately recognize `__x = __x * (double)(uVar1 ^ 0x8000000000000000);` is multiplying the summation result with negative of sigma.

And, of course, it is obvious what `__x = exp(__x);` means. You can get the formula `e^(-sigma * SSE)` and verify it by doing some Python experiments.

So, a possible implementation of `sim` is

```python=
def sim(x1, x2, sigma):
    return np.exp(-sigma * np.sum(np.square(x1 - x2), keepdims=True))
```


### Hyperparameter Tuning

Results show sigma values like `1e-4`, `1e-5`, `1e-6` work well. This need some manual trial and error. One tip for tuning is to use log scale (recall what you have decompiled).


### Guessing the Flag

Let's assume there is no randomness applied. Now, our task would be finding the maximum point in a convex surface (recall the properties of L2 distance). You can apply your favourite learning/optimization algorithm(s) to find the flag.

One method we can use is Powell’s method (taught in COMP 4421). Although it got a fancy name, it is a simple algorithm that everyone should be able to think of.

```python=
guess = "flag{12345678901234567}"
cur_sim = ask(guess, 1e-4)

for i in range(len(guess)):
    def try_letter(o):
        c = chr(o)
        att = guess[:i] + c + guess[i+1:]
        res = ask(att, 1e-4)
        return att, res
    lo, hi = 32, 128
    for o in range(lo, hi):
        att, res = try_letter(o)
        if res > cur_sim:
            guess = att
            cur_sim = res
```


### Dealing with Randomness

Instead of a fix number, we get a number sampled from a random distribution. There is a critical observation, we don't need to estimate the parameters of the distribution (or the actual simiarity not scaled by randomness) to solve the task. We only need to know which distribution tends to give larger numbers.

Taking arithemetic mean of the 10 identical query works well. To implement that, one can write a simple wraper that wraps around the original query function.

```python=
def ask2(flag, sigma, iters=10):
    vals = []
    for _ in range(iters):
        vals.append(ask(flag, sigma))
    return statistics.fmean(vals)
```

Please be noted that this is not a good way to estimate the actual similarity value. (You may want to use geometric mean instead.)

Another problem is the character fails to coverge to the exact maximum when the difference of other characters dominates. In this case, the variation due to randomness outplayed the small change of the character. Luckily, that also means it is close to the ground truth character. A simple but effective fix would be running the algorithm more than once to iteratively improve the solution. The offical solution uses 5 iterations.


### Optimization

Linear search may takes up lot of queries. Although there is nothing that stops you sending lots of queries, it would take quite a while to complete.

```python=
# linear search
lo, hi = 32, 128
for o in range(lo, hi):
    att, res = try_letter(o)
    if res > cur_sim:
        guess = att
        cur_sim = res
```

To solve this, note that the function that we optimize a single character is unimodel. We can apply ternary search.

```python=
# ternary search
lo, hi = 32, 128
while hi - lo >= 3:
    m1 = lo + (hi - lo) // 3
    m2 = hi - (hi - lo) // 3
    a1, r1 = try_letter(m1)
    a2, r2 = try_letter(m2)
    if r1 < r2:
        lo = m1
    else:
        hi = m2

# linear search
for o in range(lo, hi + 1):
    att, res = try_letter(o)
    if res > cur_sim:
        guess = att
        cur_sim = res
```

Optionally, you may want to cache some of the queries (to save queries/improve quality of estimates).

```python=
cache = dict()
def try_letter(o):
    if o in cache:
        return cache[o]
    c = chr(o)
    att = guess[:i] + c + guess[i+1:]
    res = ask2(att, 1e-4)
    cache[o] = att, res
    return att, res
```

The official solution uses <20000 queries. I suggest adding a progress/partial solution display as the solution usually converges in the middle of the run (you don't need to wait all queries to finish).


## Potential Issues

### Fairness

This may seem unfair to those who have no taken COMP 4211. However, knowing the function in blackbox would not bring huge advantage. The keys to solve this chall are 

a. knowing the function behaves exponenetially; and
b. the optimization surface is convex.

That is difficult to observe without lots of visualization (probably no one would do that in the assignment) or decompilation. It also require skills to optimize and find the solution (like you do in a blind SQL injection).


### Advanced Techniques

Some may concern skills required for the chall is too difficult. It's not. TRY HARDER works. :)


#### Optimization Algorithms

It seems the optimation method is new. But if you recall how you would perform a blind SQL injection, it shares a lot of similarity.


#### Ternary Search

Binary search would also work. One think to note for binary search is the condition `f(mi) > f(mi + 1)` might not work due to randomness. One may try `f(mi) > f(mi + 5)` or similar and do a linear search.


## Author

- Tommy LI (Discord: STommydx#0954)
- Credit to COMP 4211 TAs for providing similarity blackbox

###### tags: `ctf`
