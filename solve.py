from pwn import *
import math
import statistics

with process(['python', 'chall.py']) as r:  # replace this as remote

    # headers
    r.recvline()
    r.recvline()
    r.recvline()
    r.recvline()

    guess_cnt = 0

    def ask(flag, sigma):
        global guess_cnt
        guess_cnt = guess_cnt + 1
        r.recvuntil(' ')  # flag
        r.sendline(flag)
        r.recvuntil(' ')  # sigma
        r.sendline(str(sigma))
        return float(r.recvline())

    def ask2(flag, sigma, iters=10):
        vals = []
        for _ in range(iters):
            vals.append(ask(flag, sigma))
        return statistics.fmean(vals)

    guess = "flag{12345678901234567}"
    cur_sim = ask2(guess, 1e-4)

    epochs = 5
    for _ in range(epochs):
        for i in range(len(guess)):

            cache = dict()
            def try_letter(o):
                if o in cache:
                    return cache[o]
                c = chr(o)
                att = guess[:i] + c + guess[i+1:]
                res = ask2(att, 1e-4)
                cache[o] = att, res
                return att, res

            # ternary search
            lo, hi = 32, 128
            while hi - lo >= 3:
                m1 = lo + (hi - lo) // 3
                m2 = hi - (hi - lo) // 3
                a1, r1 = try_letter(m1)
                a2, r2 = try_letter(m2)
                if r1 < r2:
                    lo = m1
                else:
                    hi = m2

            # linear search
            for o in range(lo, hi + 1):
                att, res = try_letter(o)
                if res > cur_sim:
                    guess = att
                    cur_sim = res

    print(guess)
    print(guess_cnt)

